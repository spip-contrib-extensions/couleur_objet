<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function couleur_objet_affiche_droite($flux) {

	if (
		($e = trouver_objet_exec($flux['args']['exec']))
		and ($table_objet_sql = $e['table_objet_sql'])
	) {
		include_spip('inc/config');
		$objets_config = lire_config('couleur_objet/objets', array());

		if (
			in_array($table_objet_sql, $objets_config) // si configuration objets ok
			and ($e !== false) // page d'un objet éditorial
			and ($e['edition'] === false) // pas en mode édition
			and ($id_objet = $flux['args'][$e['id_table_objet']])
		) {
			$objet = $e['type'];
			include_spip('inc/couleur_objet');
			$couleur_objet = objet_lire_couleur($objet, $id_objet);
			$contexte = array('objet' => $objet, 'id_objet' => $id_objet, 'couleur_objet' => $couleur_objet);
			$flux['data'] .= recuperer_fond('inclure/couleur_objet', $contexte);
		}
	}

	return $flux;
}
